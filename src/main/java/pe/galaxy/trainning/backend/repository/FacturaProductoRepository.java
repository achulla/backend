package pe.galaxy.trainning.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.galaxy.trainning.backend.entity.FacturaProducto;
import pe.galaxy.trainning.backend.entity.FacturaProductoId;

@Repository
public interface FacturaProductoRepository extends JpaRepository<FacturaProducto, FacturaProductoId> {


}
