package pe.galaxy.trainning.backend.entity;

import jakarta.persistence.*;
import lombok.*;


@Getter
@Setter
@Entity
@Table(name = "facturas_productos")
public class    FacturaProducto {

    @EmbeddedId
    private FacturaProductoId id;

    @ManyToOne
    @MapsId("facturaId")
    private Factura factura;

    @ManyToOne
    @MapsId("productoId")
    private Producto producto;

    @Column
    private Double subTotal;

    @Column
    private Integer cantidad;

    public FacturaProducto() {

    }

    public FacturaProducto(Factura factura, Producto producto, Double subTotal, Integer cantidad) {
        this.setId(new FacturaProductoId(factura.getId(), producto.getId()));
        this.producto = producto;
        this.factura = factura;
        this.subTotal = subTotal;
        this.cantidad = cantidad;
    }

}
