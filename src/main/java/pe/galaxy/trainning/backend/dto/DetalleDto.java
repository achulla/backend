package pe.galaxy.trainning.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DetalleDto {

    private Long id;
    @NotBlank(message = "descripcion producto es obligatorio")
    private String descripcionProducto;
    @NotBlank(message = "cantidad es obligatorio")
    private Integer cantidad;
    private Double subTotal;
}
