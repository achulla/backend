-- Insertando 10 registros de Clientes
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (90, 'Miguel', '987654562', 'Miguel@email.com', 'Ca Cuzco 333');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (91, 'Pedro', '977654562', 'Pedro@email.com', 'Ca Gremio 134');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (92, 'Mario', '937554562', 'Mario@email.com', 'Jr Condor 1345');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (93, 'Daniel', '917254562', 'Daniel@email.com', 'Ca Cuzco 333');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (94, 'Alfonso', '911654562', 'Alfonso@email.com', 'Av Flores 654');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (95, 'Cindy', '937654234', 'Cindy@email.com', 'Ca Geranios 875');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (96, 'Silvia', '954664562', 'Silvia@email.com', 'Ca Arenales 907');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (97, 'Veronica', '977657561', 'Veronica@email.com', 'Ca Robles 885');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (98, 'Angel', '923654569', 'Angel@email.com', 'Jr Pardo 567');
INSERT INTO clientes (id, nombres, telefono, correo, direccion) VALUES (99, 'Luis', '985554562', 'Luis@email.com', 'Av Salaverry 465');


-- Insertando 6 categorias
INSERT INTO categorias (id, descripcion) values (1, 'Movil');
INSERT INTO categorias (id, descripcion) values (2, 'Memoria');
INSERT INTO categorias (id, descripcion) values (3, 'Laptop');
INSERT INTO categorias (id, descripcion) values (4, 'Disco');
INSERT INTO categorias (id, descripcion) values (5, 'Parlantes & Audifonos');
INSERT INTO categorias (id, descripcion) values (6, 'Refrigeracion');

-- Insertando 10 registros de Productos
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1000, 'Samsung S10', 10, 1200.00, 1);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1001, 'Iphone XS', 10, 3200.00, 1);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1002, 'Motorola GPlus', 10, 1100.00, 1);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1003, 'Disco Duro Toshiba', 50, 300.00, 4);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1004, 'USB Kingston 16GB', 60, 25.00, 2);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1005, 'USB HP 32 GB', 30, 35.00, 2);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1006, 'Laptop HP Corei3', 30, 1150.00, 3);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1007, 'Audifonos Logitech XTL', 10, 80.00, 5);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1008, 'Parlantes Logitech', 15, 90.00, 5);
INSERT INTO productos (id, descripcion, stock, precio, categoria_id) VALUES (1009, 'Cooler Marca XYZ', 25, 110.00, 6);