package pe.galaxy.trainning.backend.repository;

import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.galaxy.trainning.backend.entity.Categoria;
import pe.galaxy.trainning.backend.entity.Producto;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CategoriaRepositoryTest {

    @Autowired
    public CategoriaRepository repository;

    @Test
    public void guardarCategoriaTest(){
        Categoria categoria = new Categoria();
        categoria.setDescripcion("Cables");
        repository.save(categoria);
    }

    @Test
    public void obtenerProductosPorCategoriaTest(){
        Categoria categoria = repository.obtenerProductosPorCategoria(1L);
        assertEquals( 3 , categoria.getProductos().size());
    }

    @Test
    public void obtenerCategoriaTest() {

        Throwable throwable = assertThrows( Throwable.class, () -> {
            Categoria categoria = repository.findById(1L).get();
            Set<Producto> productos = categoria.getProductos();
            assertEquals( 3 , productos.size());
        });

        assertEquals(LazyInitializationException.class, throwable.getClass());
    }
}
