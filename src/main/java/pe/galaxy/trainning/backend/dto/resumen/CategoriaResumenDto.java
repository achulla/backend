package pe.galaxy.trainning.backend.dto.resumen;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriaResumenDto {
    private Long id;
    @NotBlank(message = "{descricpion.notnull}")
    private String descripcion;
}
