package pe.galaxy.trainning.backend.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoriaRequestDto {

    private String descripcion;
}
