package pe.galaxy.trainning.backend.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pe.galaxy.trainning.backend.dto.AuthResponseDto;
import pe.galaxy.trainning.backend.dto.LoginRequestDto;
import pe.galaxy.trainning.backend.dto.RegistroRequestDto;
import pe.galaxy.trainning.backend.entity.User;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;
import pe.galaxy.trainning.backend.repository.UserRepository;

@AllArgsConstructor
@Service
public class AuthService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;

    public AuthResponseDto login(LoginRequestDto requestDto){

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestDto.getUsername(), requestDto.getPassword()));
        UserDetails user=userRepository.findByUsername(requestDto.getUsername()).orElseThrow();
        String token=jwtService.getToken(user);
        return AuthResponseDto.builder()
                .token(token)
                .build();
    }

    public AuthResponseDto registro(RegistroRequestDto requestDto){
        User user = User.builder()
                .username(requestDto.getUsername())
                .password(passwordEncoder.encode( requestDto.getPassword()))
                .nombres(requestDto.getNombres())
                .role(requestDto.getRole())
                .build();

        userRepository.save(user);
        return AuthResponseDto.builder().token(jwtService.getToken(user)).build();
    }
}
