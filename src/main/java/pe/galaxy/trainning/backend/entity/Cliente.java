package pe.galaxy.trainning.backend.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity // ORM
@Table(name = "clientes")
public class Cliente {

    @Id
    @SequenceGenerator(name = "cliente_sequence", sequenceName = "cliente_sequence", initialValue = 100, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cliente_sequence")
    @Column(name = "id")
    private Long id;
    @Column(name = "nombres", nullable = false)
    private String nombres;
    @Column(name = "telefono", nullable = false)
    private String telefono;
    @Column(name = "correo", nullable = false)
    private String correo;
    @Column(name = "direccion", nullable = false)
    private String direccion;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
    private Set<Factura> facturas = new HashSet<>();
}
