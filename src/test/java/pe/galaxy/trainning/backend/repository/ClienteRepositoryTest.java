package pe.galaxy.trainning.backend.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.galaxy.trainning.backend.entity.Cliente;
import pe.galaxy.trainning.backend.entity.Factura;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private FacturaRepository facturaRepository;

    @Test
    public void registrarClienteTest(){
        var cliente = new Cliente();
        cliente.setNombres("Jorge");
        cliente.setTelefono("996784512");
        cliente.setCorreo("Jorge@gmail.com");
        cliente.setDireccion("Lima");

        Date date = new Date();
        var factura = new Factura();
        factura.setFecha(date);
        factura.setHora(date);
        factura.setImporteTotal(10.00);
        factura.setCliente(cliente);

        cliente.setFacturas(Set.of(factura));
        clienteRepository.save(cliente);

        facturaRepository.save(factura);

        List<Cliente> clienteList = clienteRepository.findAll();
        clienteList.forEach(p-> System.out.println(cliente.toString()));

        assertEquals(1,clienteList.size());
    }

}
