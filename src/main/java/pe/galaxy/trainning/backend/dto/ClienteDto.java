package pe.galaxy.trainning.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClienteDto {

    private Long id;

    @NotBlank(message = "{nombres.notblank}")
    private String nombres;

    @NotBlank(message = "Telefono es obligatorio")
    private String telefono;

    @Email(message = "No es un correo")
    @NotBlank(message = "Correo es obligatorio")
    private String correo;

    @NotBlank(message = "Direccion es obligatorio")
    private String direccion;

    private Set<FacturaDto> facturas = new HashSet<>();
}
