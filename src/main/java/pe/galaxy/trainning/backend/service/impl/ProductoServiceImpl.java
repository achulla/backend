package pe.galaxy.trainning.backend.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pe.galaxy.trainning.backend.dto.CategoriaDto;
import pe.galaxy.trainning.backend.dto.ProductoDto;
import pe.galaxy.trainning.backend.entity.Categoria;
import pe.galaxy.trainning.backend.entity.Producto;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;
import pe.galaxy.trainning.backend.repository.CategoriaRepository;
import pe.galaxy.trainning.backend.repository.ProductoRepository;
import pe.galaxy.trainning.backend.service.interfaces.ProductoService;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private final ProductoRepository productoRepository;

    @Autowired
    private final CategoriaRepository categoriaRepository;

    @Override
    public List<ProductoDto> listarProductos(String orden, String atributo, int pagina, int size) {
        Sort sort = null;
        if (String.valueOf(Sort.Direction.DESC).equalsIgnoreCase(orden)){
            sort = Sort.by(Sort.Direction.DESC, atributo);
        } else {
            sort = Sort.by(Sort.Direction.ASC, atributo);
        }

        Pageable pageable = PageRequest.of(pagina, size, sort);

        List<Producto> productoList = productoRepository.findAll(pageable).stream().toList();
        List<ProductoDto> productoDTOList = productoList.stream().map(p -> ProductoDto.builder()
                .id(p.getId())
                .descripcion(p.getDescripcion())
                .precio(p.getPrecio())
                .stock(p.getStock())
                .build()).collect(Collectors.toList());
        return productoDTOList;
    }

    @Override
    public ProductoDto obtenerProductoPorId(Long id) {
        Producto producto = productoRepository.findById(id)
                .orElseThrow(() -> new EntidadNoEncontradaException("Producto no encontrado con id: " + id));

            return ProductoDto.builder()
                    .id(producto.getId())
                    .descripcion(producto.getDescripcion())
                    .precio(producto.getPrecio())
                    .stock(producto.getStock())
                    .categoriaDto(CategoriaDto.builder()
                            .id(producto.getCategoria().getId())
                            .descripcion(producto.getDescripcion())
                            .build())
                    .build();

    }

    @Override
    public ProductoDto guardarProducto(ProductoDto productoDto) {
        Categoria categoria = categoriaRepository
                .findById(productoDto.getCategoriaDto().getId())
                .orElseThrow(() -> new EntidadNoEncontradaException("Categoria no encontrado con ID: " + productoDto.getCategoriaDto().getId()));


        Producto producto = new Producto(null, productoDto.getDescripcion(), productoDto.getStock(), productoDto.getPrecio(), categoria);
        productoRepository.save(producto);
        productoDto.setId(producto.getId());
        return productoDto;
    }

    @Override
    public void actualizarProducto(ProductoDto productoDto) {
        productoRepository.findById(productoDto.getId())
                .orElseThrow(() -> new EntidadNoEncontradaException("Producto no encontrado con ID: " + productoDto.getId()));
        Categoria categoria = categoriaRepository.findById(productoDto.getCategoriaDto().getId())
                .orElseThrow(() -> new EntidadNoEncontradaException("Categoria no encontrado con ID: " + productoDto.getCategoriaDto().getId()));

        Producto producto = new Producto(productoDto.getId(),
                productoDto.getDescripcion(),
                productoDto.getStock(),
                productoDto.getPrecio(), categoria);
        productoRepository.save(producto);
    }

    @Override
    public void eliminarProducto(Long id) {
        productoRepository.findById(id)
                .orElseThrow(() -> new EntidadNoEncontradaException("Producto no encontrado con ID: " + id));

        productoRepository.deleteById(id);
    }

    @Override
    public List<ProductoDto> obtenerProductosPorDescripcion(String descripcion) {
        List<Producto> productoList = productoRepository.findByDescripcion(descripcion);
        List<ProductoDto> productoDTOList = productoList.stream().map(p -> ProductoDto.builder()
                .id(p.getId())
                .descripcion(p.getDescripcion())
                .precio(p.getPrecio())
                .stock(p.getStock())
                .build()).collect(Collectors.toList());
        return productoDTOList;
    }
}
