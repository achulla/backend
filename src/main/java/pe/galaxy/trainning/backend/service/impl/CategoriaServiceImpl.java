package pe.galaxy.trainning.backend.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pe.galaxy.trainning.backend.dto.detalle.CategoriaDetalleDto;
import pe.galaxy.trainning.backend.dto.request.CategoriaRequestDto;
import pe.galaxy.trainning.backend.dto.resumen.CategoriaResumenDto;
import pe.galaxy.trainning.backend.dto.resumen.ProductoResumenDto;
import pe.galaxy.trainning.backend.entity.Categoria;
import pe.galaxy.trainning.backend.entity.Producto;
import pe.galaxy.trainning.backend.repository.CategoriaRepository;
import pe.galaxy.trainning.backend.service.interfaces.CategoriaService;
import pe.galaxy.trainning.backend.util.ValidacionUtil;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class CategoriaServiceImpl implements CategoriaService {

    private final CategoriaRepository categoriaRepository;
    private final ValidacionUtil validacionUtil;


    @Override
    public List<CategoriaResumenDto> listarCategorias() {
        List<Categoria> categoriaList = categoriaRepository.findAll();
        List<CategoriaResumenDto> categoriaDTOList = new ArrayList<>();
        for (Categoria categoria: categoriaList ){
            CategoriaResumenDto dto = new CategoriaResumenDto();
            dto.setId(categoria.getId());
            dto.setDescripcion(categoria.getDescripcion());

            categoriaDTOList.add(dto);
        }

        return categoriaDTOList;
    }

    @Override
    public CategoriaResumenDto obtenerCategoriaPorId(Long id) {
        Categoria categoria = validacionUtil.encontrarCategoriaPor(id);
        CategoriaResumenDto dto = new CategoriaResumenDto();
        dto.setId(categoria.getId());
        dto.setDescripcion(categoria.getDescripcion());

        return dto;
    }

    @Override
    public CategoriaResumenDto guardarCategoria(CategoriaRequestDto requestDto) {
        Categoria categoria = new Categoria();
        categoria.setDescripcion(requestDto.getDescripcion());
        categoriaRepository.save(categoria);

        CategoriaResumenDto resumenDto = new CategoriaResumenDto();
        resumenDto.setId(categoria.getId());
        resumenDto.setDescripcion(categoria.getDescripcion());
        return resumenDto;
    }

    @Override
    public void actualizarCategoria(Long id, CategoriaRequestDto requestDto) {
        Categoria categoria = validacionUtil.encontrarCategoriaPor(id);
        categoria.setDescripcion(requestDto.getDescripcion());
        categoriaRepository.save(categoria);
    }

    @Override
    public void eliminarCategoria(Long id) {
        Categoria categoria = validacionUtil.encontrarCategoriaPor(id);
        categoriaRepository.delete(categoria);
    }

    @Override
    public CategoriaDetalleDto obtenerProductosPorCategoria(Long id) {
        Categoria categoria = categoriaRepository.obtenerProductosPorCategoria(id);
        CategoriaDetalleDto detalleDto = new CategoriaDetalleDto();
        detalleDto.setId(categoria.getId());
        detalleDto.setDescripcion(categoria.getDescripcion());
        List<ProductoResumenDto> productos = new ArrayList<>();

        for (Producto producto: categoria.getProductos()){
            ProductoResumenDto dto = new ProductoResumenDto();
            dto.setDescripcion(producto.getDescripcion());
            dto.setStock(producto.getStock());
            dto.setPrecio(producto.getPrecio());

            productos.add(dto);
        }

        detalleDto.setProductos(productos);
        return detalleDto;
    }
}
