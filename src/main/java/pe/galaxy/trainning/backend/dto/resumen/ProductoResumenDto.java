package pe.galaxy.trainning.backend.dto.resumen;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductoResumenDto {

    @NotBlank(message = "descripcion es obligatorio")
    private String descripcion;

    @NotBlank(message = "stock es obligatorio")
    private Integer stock;

    @NotBlank(message = "precio es obligatorio")
    private Double precio;
}
