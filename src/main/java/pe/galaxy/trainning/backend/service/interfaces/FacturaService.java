package pe.galaxy.trainning.backend.service.interfaces;

import pe.galaxy.trainning.backend.dto.FacturaDto;

import java.util.List;

public interface FacturaService {
    List<FacturaDto> listarFacturas();

    FacturaDto obtenerFacturaPorId(Long id);

    FacturaDto guardarFactura(FacturaDto facturaDto);

    void eliminarFactura(Long id);
}
