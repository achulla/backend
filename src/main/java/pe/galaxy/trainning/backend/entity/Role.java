package pe.galaxy.trainning.backend.entity;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
