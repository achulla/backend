package pe.galaxy.trainning.backend.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.galaxy.trainning.backend.entity.Cliente;
import pe.galaxy.trainning.backend.entity.Factura;
import pe.galaxy.trainning.backend.entity.FacturaProducto;
import pe.galaxy.trainning.backend.entity.Producto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class FacturaRepositoryTest {

    @Autowired
    private FacturaRepository facturaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private FacturaProductoRepository facturaProductoRepository;

    @Test
    public void registrarFacturaTest(){
        Date fechaHora = new Date();
        var factura = new Factura();
        factura.setFecha(fechaHora);
        factura.setHora(fechaHora);
        factura.setImporteTotal(10.00);

        facturaRepository.save(factura);
        assertEquals(10.00, facturaRepository.findById(factura.getId()).get().getImporteTotal());
    }

    @Test
    public void registrarFacturaDetalleTest() {
        var cliente = new Cliente(null, "Miguel", "999888444", "miguel@email.com", "Ca Aguilas 285", null );
        clienteRepository.save(cliente);

        var producto = new Producto(null, "Iphone XS", 10, 2500.00, null);
        productoRepository.save(producto);

        Date d = new Date();

        Factura factura = new Factura(null, d, d, 2500.00, cliente, null);

        facturaRepository.save(factura);

        FacturaProducto fp = new FacturaProducto(factura, producto, 2500.00, 1);

        facturaProductoRepository.save(fp);

        facturaRepository.findAll();


    }

}
