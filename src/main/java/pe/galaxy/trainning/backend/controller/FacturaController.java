package pe.galaxy.trainning.backend.controller;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pe.galaxy.trainning.backend.dto.FacturaDto;
import pe.galaxy.trainning.backend.service.interfaces.FacturaService;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/facturas")
public class FacturaController {

    @Autowired
    private FacturaService facturaService;

    @GetMapping()
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<List<FacturaDto>> obtenerFacturas() {
        List<FacturaDto> facturaDTOList = facturaService.listarFacturas();
        return new ResponseEntity<>(facturaDTOList, HttpStatus.OK);
    }

    @GetMapping( "/{id}")
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<FacturaDto> obtenerFacturaPorId(@PathVariable long id) {
        log.info("Obtener factura por ID: " + id);
        var facturaDTO = facturaService.obtenerFacturaPorId(id);
        return new ResponseEntity<>(facturaDTO, HttpStatus.OK);
    }

    @PostMapping()
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<FacturaDto> guardarFactura(@Valid @RequestBody FacturaDto facturaDto) {
        log.info("Factura: " + facturaDto);
        return new ResponseEntity<>(facturaService.guardarFactura(facturaDto), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> eliminarFactura(@PathVariable long id) {
       facturaService.eliminarFactura(id);
       return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
