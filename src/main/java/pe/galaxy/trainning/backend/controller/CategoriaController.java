package pe.galaxy.trainning.backend.controller;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pe.galaxy.trainning.backend.dto.CategoriaDto;
import pe.galaxy.trainning.backend.dto.detalle.CategoriaDetalleDto;
import pe.galaxy.trainning.backend.dto.request.CategoriaRequestDto;
import pe.galaxy.trainning.backend.dto.resumen.CategoriaResumenDto;
import pe.galaxy.trainning.backend.service.interfaces.CategoriaService;

import java.util.List;
@Slf4j
@RestController
@RequestMapping("/api/v1/categorias")
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    @GetMapping
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<List<CategoriaResumenDto>> obtenerCategorias() {
        log.info("Obtener categorias ...");
        return new ResponseEntity<>(categoriaService.listarCategorias(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<CategoriaResumenDto> obtenerCategoriaPorId(@PathVariable long id) {
        log.info("Obtener categoria por ID: " + id);
        var categoriaDTO = categoriaService.obtenerCategoriaPorId(id);
        return new ResponseEntity<>(categoriaDTO, HttpStatus.OK);
    }

    @GetMapping("/{id}/productos")
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<CategoriaDetalleDto> obtenerProductosPorCategoria(@PathVariable long id) {
        log.info("Obtener productos por Categoria ID: " + id);
        var categoriaDTO = categoriaService.obtenerProductosPorCategoria(id);
        return new ResponseEntity<>(categoriaDTO, HttpStatus.OK);
    }

    @PostMapping
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<CategoriaResumenDto> guardarCategoria(@Valid @RequestBody CategoriaRequestDto requestDto) {
        log.info("Guardar categoria: " + requestDto);
        return new ResponseEntity<>(categoriaService.guardarCategoria(requestDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> actualizarCategoria(@PathVariable Long id,
                                                    @RequestBody CategoriaRequestDto requestDto ) {
        log.info("Actualizar categoria: " + requestDto);
        categoriaService.actualizarCategoria(id, requestDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> eliminarCategoria(@PathVariable long id) {
        log.info("Eliminar cliente ID: " + id);
        categoriaService.eliminarCategoria(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
