package pe.galaxy.trainning.backend.util;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pe.galaxy.trainning.backend.entity.Categoria;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;
import pe.galaxy.trainning.backend.repository.CategoriaRepository;

@AllArgsConstructor
@Component
public class ValidacionUtil {
    private final CategoriaRepository categoriaRepository;

    public Categoria encontrarCategoriaPor(Long id){
        return categoriaRepository.findById(id).orElseThrow(
                () -> new EntidadNoEncontradaException("No se encontro la categoria con id " + id));
    }
}
