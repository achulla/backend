package pe.galaxy.trainning.backend.service.impl;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.galaxy.trainning.backend.dto.ClienteDto;
import pe.galaxy.trainning.backend.entity.Cliente;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;
import pe.galaxy.trainning.backend.repository.ClienteRepository;
import pe.galaxy.trainning.backend.service.interfaces.ClienteService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private final ClienteRepository clienteRepository;

    @Override
    public List<ClienteDto> listarClientes() {
        List<Cliente> clientes = clienteRepository.findAll();
        List<ClienteDto> dtoList = clientes.stream().map(p -> ClienteDto.builder().id(p.getId())
                .nombres(p.getNombres())
                .telefono(p.getTelefono())
                .correo(p.getCorreo())
                .direccion(p.getDireccion()).build()).collect(Collectors.toList());

        return dtoList;
    }

    @Override
    public ClienteDto obtenerClientePorId(Long id) {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(
                () -> new EntidadNoEncontradaException("Cliente no encontrado con ID: " + id));

            return ClienteDto.builder().id(cliente.getId())
                    .nombres(cliente.getNombres())
                    .correo(cliente.getCorreo())
                    .telefono(cliente.getTelefono())
                    .direccion(cliente.getDireccion()).build();
    }

    @Override
    public ClienteDto guardarCliente(ClienteDto clienteDto) {
        var cliente = new Cliente();
        cliente.setNombres(clienteDto.getNombres());
        cliente.setTelefono(clienteDto.getTelefono());
        cliente.setCorreo(clienteDto.getCorreo());
        cliente.setDireccion(clienteDto.getDireccion());
        cliente.setFacturas(null);
        clienteRepository.save(cliente);
        clienteDto.setId(cliente.getId());
        return clienteDto;
    }

    @Override
    public void actualizarCliente(ClienteDto clienteDto) {
      Cliente cliente =  clienteRepository.findById(clienteDto.getId()).orElseThrow(
                () -> new EntidadNoEncontradaException("Clinete no encontrado con ID: " + clienteDto.getId()));

        cliente.setNombres(clienteDto.getNombres());
        cliente.setTelefono(clienteDto.getTelefono());
        cliente.setCorreo(clienteDto.getCorreo());
        cliente.setDireccion(clienteDto.getDireccion());
        cliente.setFacturas(null);

        clienteRepository.save(cliente);
    }

    @Override
    public void eliminar(Long id) {
        Cliente cliente =  clienteRepository.findById(id).orElseThrow(
                () -> new EntidadNoEncontradaException("Clinete no encontrado con ID: " + id));

        clienteRepository.delete(cliente);
    }

    @Override
    public List<ClienteDto> listarClientesPorNombre(String nombre) {
        List<Cliente> clientes = clienteRepository.findByNombres(nombre.toLowerCase());
        List<ClienteDto> dtoList = clientes.stream().map(p -> ClienteDto.builder()
                .id(p.getId())
                .nombres(p.getNombres())
                .telefono(p.getTelefono())
                .correo(p.getCorreo())
                .direccion(p.getDireccion()).build()).collect(Collectors.toList());

        return dtoList;
    }
}
