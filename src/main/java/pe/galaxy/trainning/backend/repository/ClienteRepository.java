package pe.galaxy.trainning.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.galaxy.trainning.backend.entity.Cliente;

import java.util.List;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Query("select c from Cliente c where lower(c.nombres)  LIKE %?1%")
    List<Cliente> findByNombres(String nombre);
}
