package pe.galaxy.trainning.backend.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import pe.galaxy.trainning.backend.dto.ClienteDto;
import pe.galaxy.trainning.backend.entity.Cliente;
import pe.galaxy.trainning.backend.service.interfaces.ClienteService;

import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {

    @Autowired
    private final ClienteService clienteService;

    @GetMapping()
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<List<ClienteDto>> obtenerCliente(@RequestParam(required = false) String nombre){
        List<ClienteDto> clienteDtos = null;
        if (StringUtils.hasLength(nombre)){
            clienteDtos = clienteService.listarClientesPorNombre(nombre);
        }
        else {
            clienteDtos =  clienteService.listarClientes();
        }
        return new ResponseEntity<>(clienteDtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<ClienteDto> obtenerClientePorId(@PathVariable("id") Long id){
     return new ResponseEntity<>(clienteService.obtenerClientePorId(id), HttpStatus.OK);
    }

    // ResponseEntity -> para tener un control con las respuestas http status
    @PostMapping()
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<ClienteDto> crearCliente(@Valid @RequestBody ClienteDto clienteDto){
      return new ResponseEntity<>(clienteService.guardarCliente(clienteDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> actualizarCliente(@RequestBody ClienteDto clienteDto,
                                     @PathVariable("id") Long id){
        clienteDto.setId(id);
        clienteService.actualizarCliente(clienteDto);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void>  eliminarCliente(@PathVariable("id") Long id){
        clienteService.eliminar(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
