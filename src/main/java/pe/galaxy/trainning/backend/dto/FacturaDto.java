package pe.galaxy.trainning.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FacturaDto {

    private Long id;

    private Date fecha;

    private Date hora;

    private Double importeTotal;

    @NotBlank(message = "Nombre es obligatorio")
    private ClienteDto clienteDto;

    @NotNull(message = "detalle no debes nulo")
    @Size(message = "detalles debe ser mayor a un registro", min = 1)
    private List<DetalleDto> detalles = new ArrayList<>();
}
