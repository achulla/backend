package pe.galaxy.trainning.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL) // para el response
public class ProductoDto {

    private Long id;

    @NotBlank(message = "descripcion es obligatorio")
    private String descripcion;

    @NotBlank(message = "stock es obligatorio")
    private Integer stock;

    @NotBlank(message = "precio es obligatorio")
    private Double precio;

    @NotNull(message = "{categoria.notnull}")
    private CategoriaDto categoriaDto;
}
