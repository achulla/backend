package pe.galaxy.trainning.backend.dto;

import lombok.*;
import pe.galaxy.trainning.backend.entity.Role;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class RegistroRequestDto {

    private String username;
    private String password;
    private String nombres;
    private Role role;
}
