package pe.galaxy.trainning.backend.service.interfaces;

import pe.galaxy.trainning.backend.dto.detalle.CategoriaDetalleDto;
import pe.galaxy.trainning.backend.dto.request.CategoriaRequestDto;
import pe.galaxy.trainning.backend.dto.resumen.CategoriaResumenDto;

import java.util.List;

public interface CategoriaService {

    List<CategoriaResumenDto> listarCategorias();

    CategoriaResumenDto obtenerCategoriaPorId(Long id);

    CategoriaResumenDto guardarCategoria(CategoriaRequestDto requestDto);

    void actualizarCategoria(Long id, CategoriaRequestDto requestDto);

    void eliminarCategoria(Long id);

    CategoriaDetalleDto obtenerProductosPorCategoria(Long id);
}
