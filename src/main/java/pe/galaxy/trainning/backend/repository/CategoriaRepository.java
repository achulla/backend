package pe.galaxy.trainning.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.galaxy.trainning.backend.entity.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

    @Query("select c from Categoria c JOIN fetch c.productos p where c.id =:categoriaId")
    Categoria obtenerProductosPorCategoria(@Param("categoriaId") Long categoriaId);
}
