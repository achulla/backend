package pe.galaxy.trainning.backend.service.interfaces;

import pe.galaxy.trainning.backend.dto.ClienteDto;

import java.util.List;

public interface ClienteService {

    List<ClienteDto> listarClientes();

    ClienteDto obtenerClientePorId(Long id);

    ClienteDto guardarCliente(ClienteDto clienteDto);

    void actualizarCliente(ClienteDto clienteDto);

    void eliminar(Long id);

    List<ClienteDto> listarClientesPorNombre(String nombre);
}
