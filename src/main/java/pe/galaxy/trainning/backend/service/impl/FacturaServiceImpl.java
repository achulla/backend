package pe.galaxy.trainning.backend.service.impl;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.galaxy.trainning.backend.dto.ClienteDto;
import pe.galaxy.trainning.backend.dto.DetalleDto;
import pe.galaxy.trainning.backend.dto.FacturaDto;
import pe.galaxy.trainning.backend.entity.Cliente;
import pe.galaxy.trainning.backend.entity.Factura;
import pe.galaxy.trainning.backend.entity.FacturaProducto;
import pe.galaxy.trainning.backend.entity.Producto;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;
import pe.galaxy.trainning.backend.repository.ClienteRepository;
import pe.galaxy.trainning.backend.repository.FacturaProductoRepository;
import pe.galaxy.trainning.backend.repository.FacturaRepository;
import pe.galaxy.trainning.backend.repository.ProductoRepository;
import pe.galaxy.trainning.backend.service.interfaces.FacturaService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FacturaServiceImpl implements FacturaService {

    @Autowired
    private FacturaRepository facturaRepository;

    @Autowired
    private FacturaProductoRepository facturaProductoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ProductoRepository productoRepository;

    @Override
    public List<FacturaDto> listarFacturas() {
        List<Factura> facturaList = facturaRepository.findAll();
        List<FacturaDto> facturaDTOList = facturaList.stream().map(f -> {
            Cliente cliente = f.getCliente();
            ClienteDto clienteDto = ClienteDto.builder()
                    .id(cliente.getId())
                    .nombres(cliente.getNombres())
                    .build();
            return FacturaDto.builder()
                    .id(f.getId())
                    .fecha(f.getFecha())
                    .hora(f.getHora())
                    .importeTotal(f.getImporteTotal())
                    .clienteDto(clienteDto)
                    .detalles(f.getFacturasProductos().stream().map(fp -> {
                        return DetalleDto.builder()
                                .cantidad(fp.getCantidad())
                                .subTotal(fp.getSubTotal())
                                .id(fp.getProducto().getId())
                                .descripcionProducto(fp.getProducto().getDescripcion())
                                .build();
                    }).collect(Collectors.toList()))
                    .build();
        }).collect(Collectors.toList());

        return facturaDTOList;
    }

    @Override
    public FacturaDto obtenerFacturaPorId(Long id) {
        Factura factura = facturaRepository.findById(id).orElseThrow(()
                -> new EntidadNoEncontradaException("Factura no encontrado con ID: " + id));

            Cliente cliente = factura.getCliente();
            ClienteDto clienteDTO = ClienteDto.builder()
                    .id(cliente.getId())
                    .nombres(cliente.getNombres())
                    .build();

            return FacturaDto.builder()
                    .id(factura.getId())
                    .fecha(factura.getFecha())
                    .hora(factura.getHora())
                    .importeTotal(factura.getImporteTotal())
                    .clienteDto(clienteDTO)
                    .detalles(factura.getFacturasProductos().stream().map(fp -> {
                        return DetalleDto.builder()
                                .cantidad(fp.getCantidad())
                                .subTotal(fp.getSubTotal())
                                .id(fp.getProducto().getId())
                                .descripcionProducto(fp.getProducto().getDescripcion())
                                .build();
                    }).collect(Collectors.toList()))
                    .build();
    }

    @Transactional
    @Override
    public FacturaDto guardarFactura(FacturaDto facturaDto) {

        Cliente cliente = clienteRepository.findById(facturaDto.getClienteDto().getId())
                .orElseThrow(() -> new EntidadNoEncontradaException("Cliente no encontrado con ID: " + facturaDto.getClienteDto().getId()));

        Date date = new Date();

        Factura factura = new Factura(null,
                date,
                date,
                null,
                cliente,
                null);
        facturaRepository.save(factura);

        var importeTotal = 0.00;

        for (DetalleDto d : facturaDto.getDetalles()) {
            Producto producto = productoRepository.findById(d.getId()).get();

            importeTotal += d.getCantidad() * producto.getPrecio();

            producto.setStock(producto.getStock() - d.getCantidad());
            productoRepository.save(producto);

            facturaProductoRepository.save(new FacturaProducto(factura, producto, d.getCantidad() * producto.getPrecio(), d.getCantidad()));
        }

        factura.setImporteTotal(importeTotal);
        facturaRepository.save(factura);

        facturaDto.setId(factura.getId());
        return facturaDto;
    }

    @Transactional
    @Override
    public void eliminarFactura(Long id) {
        Factura factura = facturaRepository.findById(id)
                .orElseThrow(() -> new EntidadNoEncontradaException("Factura no encontrado con ID: " + id));

        factura.getFacturasProductos().forEach(fp -> {
            Producto producto = productoRepository.findById(fp.getProducto().getId()).get();

            producto.setStock(producto.getStock() + fp.getCantidad());

            productoRepository.save(producto);
        });
        facturaRepository.deleteById(id);
    }
}
