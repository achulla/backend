package pe.galaxy.trainning.backend.exception;


public class EntidadNoEncontradaException extends RuntimeException{

    public EntidadNoEncontradaException(String mensaje) {
        super(mensaje);
    }
}
