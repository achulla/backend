package pe.galaxy.trainning.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pe.galaxy.trainning.backend.exception.EntidadNoEncontradaException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> catchBadRequest(MethodArgumentNotValidException ex){
        log.info(":::catchBadRequest::::");
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    public Map<String,  String> catchInternalServerError(RuntimeException ex){
            log.info("::: catchInternalServerError :::");
        Map<String, String> errors = new HashMap<>();
        errors.put("mensage", " Ocurrio un error inesperado");
        errors.put("exception", ex.toString());
        return errors;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntidadNoEncontradaException.class)
    public Map<String, String> catchEntidadNoEncontradaEx(EntidadNoEncontradaException ex){
        log.info("::: catchEntidadNoEncontradaEx :::");
        Map<String, String> errors = new HashMap<>();
        errors.put("mensaje", ex.getMessage());
        return errors;
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public Map<String, String> integrationErrorDao(DataIntegrityViolationException ex){
        log.info("::: integrationErrorDao :::");
        Map<String, String> errors = new HashMap<>();
        errors.put("mensaje","El usuario ya esta regsitrado");
        return errors;
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(BadCredentialsException.class)
    public Map<String, String> badCredentialsError(BadCredentialsException ex){
        log.info("::: badCredentialsError :::");
        Map<String, String> errors = new HashMap<>();
        errors.put("mensaje","Credenciales incorrectos");
        return errors;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AccessDeniedException.class)
    public Map<String, String> accessDeniedError(AccessDeniedException ex){
        log.info("::: accessDeniedError :::");
        Map<String, String> errors = new HashMap<>();
        errors.put("mensaje","No cuenta con el permiso para acceder a la vista");
        return errors;
    }
}
