package pe.galaxy.trainning.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoriaDto {

    private Long id;
    @NotBlank(message = "{descricpion.notnull}")
    private String descripcion;
    private List<ProductoDto> productoDtos = new ArrayList<>();

}
