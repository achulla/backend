package pe.galaxy.trainning.backend.service.interfaces;

import pe.galaxy.trainning.backend.dto.ProductoDto;

import java.util.List;

public interface ProductoService {

    List<ProductoDto> listarProductos(String orden, String atributo, int pagina, int size);

    ProductoDto obtenerProductoPorId(Long id);

    ProductoDto guardarProducto(ProductoDto producto);

    void actualizarProducto(ProductoDto producto);

    void eliminarProducto(Long id);

    List<ProductoDto> obtenerProductosPorDescripcion(String descripcion);
}
