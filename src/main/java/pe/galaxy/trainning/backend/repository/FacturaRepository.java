package pe.galaxy.trainning.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.galaxy.trainning.backend.entity.Factura;

@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long> {
}
