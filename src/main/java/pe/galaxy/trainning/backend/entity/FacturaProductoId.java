package pe.galaxy.trainning.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class FacturaProductoId implements Serializable {

    @Column(name = "factura_id")
    private Long facturaId;

    @Column(name = "producto_id")
    private Long productoId;
}
