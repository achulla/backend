package pe.galaxy.trainning.backend.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.galaxy.trainning.backend.dto.AuthResponseDto;
import pe.galaxy.trainning.backend.dto.LoginRequestDto;
import pe.galaxy.trainning.backend.dto.RegistroRequestDto;
import pe.galaxy.trainning.backend.service.impl.AuthService;

@AllArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<AuthResponseDto> login(@RequestBody LoginRequestDto requestDto){
        return ResponseEntity.ok(authService.login(requestDto));
    }

    @PostMapping("/registro")
    public ResponseEntity<AuthResponseDto> registro(@RequestBody RegistroRequestDto RequestDto){
        return ResponseEntity.ok(authService.registro(RequestDto));
    }

}
