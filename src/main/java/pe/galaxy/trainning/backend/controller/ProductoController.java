package pe.galaxy.trainning.backend.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import pe.galaxy.trainning.backend.dto.ProductoDto;
import pe.galaxy.trainning.backend.service.interfaces.ProductoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/v1/productos")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<List<ProductoDto>> obtenerProductos(@RequestParam(required = false) String descripcion,
                                                              @RequestParam(defaultValue = "asc") String orden,
                                                              @RequestParam(defaultValue = "precio") String atributo,
                                                              @RequestParam(defaultValue = "0") int pagina,
                                                              @RequestParam(defaultValue = "2") int size) {
        List<ProductoDto> productoDTOList = null;
        if (StringUtils.hasLength(descripcion)) {
            productoDTOList = productoService.obtenerProductosPorDescripcion(descripcion);
        } else {
            productoDTOList = productoService.listarProductos(orden, atributo, pagina, size);
        }
        return new ResponseEntity<>(productoDTOList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<ProductoDto> obtenerProductoPorId(@PathVariable Long id) {
        var producto = productoService.obtenerProductoPorId(id);
        return new ResponseEntity<>(producto, HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    @Secured({"USER", "ADMIN"})
    public ResponseEntity<ProductoDto> guardarProducto(@Valid @RequestBody ProductoDto productoDTO) {
        log.info("Producto: " + productoDTO);
        return new ResponseEntity<>(productoService.guardarProducto(productoDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> actualizarProducto(@PathVariable Long id, @Valid @RequestBody ProductoDto productoDTO) {
        log.info("Producto: " + productoDTO);
        productoDTO.setId(id);
        productoService.obtenerProductoPorId(id);
       return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(path = "/{id}")
    @Secured({"ADMIN"})
    public ResponseEntity<Void> eliminarProducto(@PathVariable long id) {
            productoService.eliminarProducto(id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
