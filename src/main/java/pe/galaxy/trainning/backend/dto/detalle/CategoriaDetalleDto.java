package pe.galaxy.trainning.backend.dto.detalle;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import pe.galaxy.trainning.backend.dto.ProductoDto;
import pe.galaxy.trainning.backend.dto.resumen.ProductoResumenDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CategoriaDetalleDto {

    private Long id;
    @NotBlank(message = "{descricpion.notnull}")
    private String descripcion;
    private List<ProductoResumenDto> productos = new ArrayList<>();
}
